# Requirements

- Python 3.9.x

# Usage

## With venv

```sh
python3 -m venv /path/to/new/virtual/environment #Create venv
source /path/to/new/virtual/environment/bin/activate 

cd sources
pip install -r requirements.txt

python main.py
```

## Without venv

```sh
cd sources
pip install -r requirements.txt
python main.py
```

## Generate report

```sh
cd sources
python3 backtesting.py [csv_path] #For backtesting
python3 optimized.py [csv_path] #For optimized
```

## Examples

```sh
cd sources
python3 backtesting.py datas/dataset1.csv #For backtesting
python3 optimized.py datas/actions.csv #For optimized
```
