import itertools

from helpers import limit_memory, load_actions, timing


@timing
def bruteforce(actions: list, budget: int):
    # GENERATE COMBOS
    combos = [itertools.combinations(actions, nb_actions) for nb_actions in range(1, len(actions))]

    # FLATTEN COMBOS
    combos = itertools.chain(*combos)  # Don't use list (causes OOM)
    # Iterate through each combos
    max_gain = 0
    max_price = 0
    max_gain_combo = []
    for combo in combos:
        # Continue if price exceed budget
        price = sum([action['price'] for action in combo])
        gain = sum([action['gain'] for action in combo])
        if price >= budget or gain <= max_gain:
            continue
        # Set better gain
        max_gain = gain
        max_price = price
        max_gain_combo = list(combo)

    # Show results
    print('-----BRUTEFORCE----')
    print(f"Gain: {round(sum([action['gain'] for action in max_gain_combo]), 2)}")
    print(f"Coût: {round(sum([action['price'] for action in max_gain_combo]), 2)}")
    print(f"Actions: {[action['name'] for action in max_gain_combo]}")


if __name__ == '__main__':
    limit_memory(1024 * 1024 * 1024)  # Max 1Go
    bruteforce(actions=load_actions(), budget=500)
