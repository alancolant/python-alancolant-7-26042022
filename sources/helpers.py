import csv
import sys
from functools import wraps
from time import time

import resource


# Time function call
def timing(f):
    @wraps(f)
    def wrap(*args, **kw):
        ts = time()
        result = f(*args, **kw)
        print(f'func:{f.__name__} took: {time() - ts:2.4f} sec')
        return result

    return wrap


def limit_memory(maxsize):
    soft, hard = resource.getrlimit(resource.RLIMIT_AS)
    resource.setrlimit(resource.RLIMIT_AS, (maxsize, hard))


def load_actions() -> list[dict[str, int, float]]:
    if len(sys.argv) <= 1:
        print("Veuillez spécifiez un nom de fichier")
        exit(1)
    filename = sys.argv[1]
    with open(filename, 'r', newline='') as f:
        next(f)  # To ignore first line
        return [{
            "name": name,
            "price": round(float(price), 2),
            "rent": float(profit) / 100,
            "gain": round(float(price) * float(profit) / 100, 2)
        } for [name, price, profit] in csv.reader(f) if float(price) > 0]
