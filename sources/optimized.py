"Refer to https://www.youtube.com/watch?v=wDsZhd1wEuk"

# @timing

from helpers import load_actions, limit_memory, timing


@timing
def optimized_glouton(actions: list, budget: int):
    # Sort actions by rent
    actions.sort(key=lambda x: x['rent'], reverse=True)  # chercher nLogN
    max_gain = 0
    cost = 0
    max_gain_combo = []
    for action in actions:
        if cost + action['price'] <= budget:
            max_gain_combo.append(action)
            cost += action['price']
            max_gain += action['gain']

    print('-----GLOUTON----')
    print(f"Gain: {round(sum([action['gain'] for action in max_gain_combo]), 2)}")
    print(f"Coût: {round(sum([action['price'] for action in max_gain_combo]), 2)}")
    print(f"Actions: {[action['name'] for action in max_gain_combo]}")

    # print(round(max_gain, 2), cost, [action['name'] for action in max_gain_combo])


@timing
def optimized_progressive(actions, budget):
    # Multiply because matrix incompatible with float
    coeff = 100
    budget = budget * coeff

    actions = list(map(lambda p: {**p, "price": int(p['price'] * coeff)}, actions))
    actions = [action for action in actions if 0 < action['price'] < budget and action['gain'] > 0]

    # Make matrix
    matrice = [[0] * (budget + 1)] * (len(actions) + 1)

    # Generate each line
    for idx_act in range(1, len(actions) + 1):
        matrice[idx_act] = [
            max(
                # Previous action optimized
                matrice[idx_act - 1][idx_price],
                # Actual + optimized previous for idx_price = current_price -
                matrice[idx_act - 1][idx_price - actions[idx_act - 1]['price']] + actions[idx_act - 1]['gain']
            )
            if actions[idx_act - 1]['price'] <= idx_price
            else matrice[idx_act - 1][idx_price]
            for idx_price in range(budget + 1)
        ]

    n = len(actions)
    optimized_actions = []

    while budget >= 0 and n >= 0:
        prev_action = actions[n - 1]
        if matrice[n][budget] == matrice[n - 1][budget - prev_action['price']] + prev_action['gain']:
            budget -= prev_action['price']
            optimized_actions.append(prev_action)
        n -= 1

    print('-----DYNAMIC----')
    print(f"Gain: {round(sum([action['gain'] for action in optimized_actions]), 2)}")
    print(f"Coût: {round(sum([action['price'] for action in optimized_actions]) / coeff, 2)}")
    print(f"Actions: {[action['name'] for action in optimized_actions]}")


if __name__ == '__main__':
    limit_memory(1024 * 1024 * 1024 * 4)  # Max 4Go
    # Sur le dataset 1, Sierra a pris l'action avec le plus gros gain
    # Sur le dataset 2, Sierra a suivi un algo glouton (elle a oublié le LXZU,BMHD), d'où la différence de rapport
    actions = load_actions()

    optimized_glouton(actions=actions, budget=500)
    optimized_progressive(actions=actions, budget=500)
